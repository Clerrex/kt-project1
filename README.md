# Knowledge Technologies Project 1 #

### Requires apache commons IO ###
Required for file processing and writing

### What is this repository for? ###

This project's goal is to match a review to the movie that it is about.
I've have used local edit distance to achieve this.
The percentage of the movie title that must be matched can be adjusted to improve accuracy.

### How do I get set up? ###

Simply import the project into eclipse and set the arguments as the review, the list of possible movies and the output file.