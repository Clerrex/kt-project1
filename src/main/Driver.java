/**
 * COMP30018 Knowledge Technologies
 * Project 1
 * Author: Michael Lumley (mlumley)
 */
package main;

import java.io.IOException;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.output.FileWriterWithEncoding;

/**
 * Main class to run the matching program. Uses Apache Commons IO which can be
 * found at https://commons.apache.org/
 */
public class Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String reviews = null;
		String titles = null;
		String outputFile = null;
		float threshold = 0.25f;
		int numberRuns = Integer.MAX_VALUE;
		
		String goodWords = "Data/goodWords.txt";
		String badWords = "Data/badWords.txt";

		// Process args
		if (args.length == 3) {
			reviews = args[0];
			titles = args[1];
			outputFile = args[2];
		} else if (args.length == 4) {
			reviews = args[0];
			titles = args[1];
			outputFile = args[2];
			numberRuns = Integer.parseInt(args[3]);
		} else {
			System.out.println("ERROR INVALID INPUT");
			System.exit(0);
		}

		// Read input and open output file
		InputReader in = new InputReader(reviews, titles);
		
		in.processGoodWords(goodWords);
		in.processBadWords(badWords);
		
		FileWriterWithEncoding output = null;
		try {
			output = new FileWriterWithEncoding(outputFile, Charsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}

		//matchReviews(in, numberRuns, threshold, output);
		goodReviews(in, numberRuns, output);

		// Close the output file
		try {
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Matches reviews to a film based on approximate string matching using
	 * local edit search
	 * 
	 * @param in
	 *            the inputReader that contains the reviews and titles
	 * @param numberRuns
	 *            the number of review to process
	 * @param threshold
	 *            the minimum percentage of the title that must be matched
	 * @param output
	 *            the output file to print results to
	 */
	private static void matchReviews(InputReader in, int numberRuns, float threshold, FileWriterWithEncoding output) {
		int i = 0;
		Matching match = new Matching();
		Review review;
		Review bestReview = null;
		int bestScore = 0;
		int currentScore = 0;

		while (((review = in.getReview()) != null) && i < numberRuns) {
			for (String title : in.getTitles()) {
				if ((currentScore = match.localEditDistance(title, review.getReviewContents(), threshold, -1, -1, 1,
						-1)) > bestScore) {
					bestScore = currentScore;
					bestReview = review;
					bestReview.setFilm(title);
				}
			}
			if (bestScore != 0) {
				System.out.println(
						"Review '" + bestReview.getReviewFileName() + "' is about '" + bestReview.getFilm() + "'");
				try {
					output.write("Review '" + bestReview.getReviewFileName() + "' is about '" + bestReview.getFilm()
							+ "'\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("Could not determine film for '" + review.getReviewFileName() + "'");
				try {
					output.write("Could not determine film for '" + review.getReviewFileName() + "'\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			bestScore = 0;
			currentScore = 0;
			i++;
		}
	}

	private static void goodReviews(InputReader in, int numberRuns, FileWriterWithEncoding output) {
		int i = 0;
		Matching match = new Matching();
		Review review;
		int goodScore = 0;
		int badScore = 0;
		int currentScore = 0;

		while (((review = in.getReview()) != null) && i < numberRuns) {
			for (String goodWord : in.getGoodWords()) {
				if ((currentScore = match.localEditDistance(goodWord, review.getReviewContents(), 0, -1, -1, 1,
						-1)) > goodScore) {
					goodScore = currentScore;
				}
			}
			for (String badWord : in.getBadWords()) {
				if ((currentScore = match.localEditDistance(badWord, review.getReviewContents(), 0, -1, -1, 1,
						-1)) > badScore) {
					badScore = currentScore;
				}
			}
			if (goodScore >= badScore) {
				System.out.println("Review '" + review.getReviewFileName() + "' is good");
				try {
					output.write("Review '" + review.getReviewFileName() + "' is good\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("Review '" + review.getReviewFileName() + "' is bad");
				try {
					output.write("Review '" + review.getReviewFileName() + "' is bad\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			goodScore = 0;
			badScore = 0;
			currentScore = 0;
			i++;
		}
	}

}
