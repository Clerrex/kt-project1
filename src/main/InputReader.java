/**
 * COMP30018 Knowledge Technologies
 * Project 1
 * Author: Michael Lumley (mlumley)
 */
package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;

/**
 * Reader reviews from a directory of txt files and reads film titles from a txt
 * file The film titles file must not be inside the reviews directory.
 * Uses Apache Commons IO which can be found at https://commons.apache.org/
 */
public class InputReader {

	private int index = 0;

	private File[] files;
	private FileReader title = null;
	private BufferedReader br = null;

	private ArrayList<String> titles = new ArrayList<String>();
	private ArrayList<String> goodWords = new ArrayList<String>();
	private ArrayList<String> badWords = new ArrayList<String>();

	/**
	 * Create an InputReader
	 * 
	 * @param ReviewDir
	 *            the directory containing reviews
	 * @param titlesFileName
	 *            the file containing titles
	 */
	public InputReader(String ReviewDir, String titlesFileName) {
		File folder = new File(ReviewDir);
		files = folder.listFiles();

		processTitles(titlesFileName);
	}

	/**
	 * Read from file the titles and add them to an ArrayList
	 * 
	 * @param titlesFileName
	 *            the file containing titles
	 */
	private void processTitles(String titlesFileName) {
		try {
			title = new FileReader(new File(titlesFileName));
			String line = null;
			br = new BufferedReader(title);

			while ((line = br.readLine()) != null) {
				titles.add(line);
			}
		} catch (Exception e) {
			System.out.println("ERROR FILE NOT FOUND");
			e.printStackTrace();
		}
	}

	/**
	 * Get an ArrayList of titles
	 * 
	 * @return titles
	 */
	public ArrayList<String> getTitles() {
		return titles;
	}
	
	public ArrayList<String> getGoodWords() {
		return goodWords;
	}
	
	public ArrayList<String> getBadWords() {
		return badWords;
	}

	/**
	 * Get a review
	 * 
	 * @return The review as a single string
	 */
	public Review getReview() {
		try {
			File f = files[index];
			String contents = FileUtils.readFileToString(f, Charsets.UTF_8);
			index++;
			return new Review(f.getName(), contents);
		} catch (IOException e) {
			System.out.println("ERROR FILE NOT FOUND");
			e.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e) {
			return null;
		}
		return null;
	}
	
	public void processGoodWords(String fileName) {
		try {
			FileReader goodWord = new FileReader(new File(fileName));
			String line = null;
			br = new BufferedReader(goodWord);

			while ((line = br.readLine()) != null) {
				goodWords.add(line);
			}
		} catch (Exception e) {
			System.out.println("ERROR FILE NOT FOUND");
			e.printStackTrace();
		}
	}
	
	public void processBadWords(String fileName) {
		try {
			FileReader badWord = new FileReader(new File(fileName));
			String line = null;
			br = new BufferedReader(badWord);

			while ((line = br.readLine()) != null) {
				badWords.add(line);
			}
		} catch (Exception e) {
			System.out.println("ERROR FILE NOT FOUND");
			e.printStackTrace();
		}
	}
}
