/**
 * COMP30018 Knowledge Technologies
 * Project 1
 * Author: Michael Lumley (mlumley)
 */
package main;

/**
 * Matches two strings. LocalEditDistance is from the Approximate Matching
 * lecture slides for the Smith-Waterman algorithm
 */
public class Matching {

	/**
	 * Calculates the local edit distance between two strings. From the
	 * Approximate Matching lecture slides for the Smith-Waterman algorithm
	 * 
	 * @param s1
	 *            the first string
	 * @param s2
	 *            the second string
	 * @param thresholdPrecentage
	 *            the minimum percentage of characters of s1 that the score must
	 *            be best than to be a valid score
	 * @param insertion
	 *            the value for an insertion
	 * @param deletion
	 *            the values for a deletion
	 * @param match
	 *            the value for a match
	 * @param mismatch
	 *            the value for a mismatch
	 * @return The best score for matching s1 and s2
	 */
	public int localEditDistance(String s1, String s2, float thresholdPrecentage, int insertion, int deletion,
			int match, int mismatch) {
		int lq = s1.length() + 1;
		int lt = s2.length() + 1;

		int i = 0;
		int j = 0;

		int[][] grid = new int[lq][lt];
		int max = 0;

		// Init left side to 0
		for (i = 0; i < lq; i++) {
			grid[i][0] = 0;
		}

		// Init top side to 0
		for (j = 0; j < lt; j++) {
			grid[0][j] = 0;
		}

		for (i = 1; i < lq; i++) {
			for (j = 1; j < lt; j++) {
				// 0 as a base, insertion cost, deletion cost, match/mismatch
				// cost
				grid[i][j] = max(0, grid[i - 1][j] + insertion, grid[i][j - 1] + deletion,
						grid[i - 1][j - 1] + equal(s1.charAt(i - 1), s2.charAt(j - 1), match, mismatch));
				if (grid[i][j] > max) {
					max = grid[i][j];
				}
			}
		}
		if (max < s1.length() * thresholdPrecentage)
			max = 0;
		return max;
	}

	/**
	 * Helper function for localEditDistance to determine if there is a match or
	 * mismatch between two characters
	 * 
	 * @param ch1
	 *            the first character
	 * @param ch2
	 *            the second character
	 * @param match
	 *            the value for a match
	 * @param mismatch
	 *            the value for a mismatch
	 * @return the value associated with either a match or mismatch
	 */
	private int equal(char ch1, char ch2, int match, int mismatch) {
		if (ch1 == ch2) {
			return match;
		}
		return mismatch;
	}

	/**
	 * Find the max value of four integers
	 * 
	 * @param a
	 *            the first integer
	 * @param b
	 *            the second integer
	 * @param c
	 *            the third integer
	 * @param d
	 *            the fourth integer
	 * @return the max value
	 */
	private int max(int a, int b, int c, int d) {
		int max1 = 0;
		int max2 = 0;

		max1 = Math.max(a, b);
		max2 = Math.max(c, d);

		return Math.max(max1, max2);
	}

}
