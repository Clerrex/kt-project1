/**
 * COMP30018 Knowledge Technologies
 * Project 1
 * Author: Michael Lumley (mlumley)
 */
package main;

/**
 * Contains information about a review
 */
public class Review {

	private String reviewFileName;
	private String film;
	private String contents;

	/**
	 * Create a review
	 * 
	 * @param fileName
	 *            the file name of the review
	 * @param contents
	 *            the review text
	 */
	public Review(String fileName, String contents) {
		this.reviewFileName = fileName;
		this.contents = contents;
	}

	/**
	 * @return the review
	 */
	public String getReviewFileName() {
		return reviewFileName;
	}

	/**
	 * @return the contents
	 */
	public String getReviewContents() {
		return contents;
	}

	/**
	 * @return the film
	 */
	public String getFilm() {
		return film;
	}

	/**
	 * @param film
	 *            the film to set
	 */
	public void setFilm(String film) {
		this.film = film;
	}
}
